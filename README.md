# The weather forecast #

In the project was a used library *Rain & Water Effect*

Read more here: [License](https://github.com/codrops/RainEffect/)


For weather forecasts used API [openweathermap.org](https://openweathermap.org)

The forecast in the free version of OpenWeatherMap.org is possible for 5 days, and update - every 3 hours.

Integrate or build upon it for free in your personal or commercial projects.

#Install
Clone this repository.

Get **Api Key** in https://openweathermap.org

The Key is inserted into a constant APIKey = "" (in common.js)


![img.jpg](https://bitbucket.org/repo/z8Xzgee/images/409149408-img.jpg)


#Settings
All settings in common.js.

Constant "Unit" - responsible for the temperature system.

Constant "Protocol" - responsible for the protocol to exchange. If you don't have SSL certificate(https), then HTML5 Geolocation API won't be available in WebKit browsers.
Therefore the script itself switches to the location identified by IP address(used [ipinfo.io](http://ipinfo.io)).

Constants "weekdays" and "months" - define the name of the days of the week and months.

[Demo](http://forecast.eu.pn/)