/**
 * Created by Kostia N. on 05.06.2017.
 */
 'use strict';

const APIKey = ""; // !!!
const Unit = "metric"; // metric: Celsius, standard: Kelvin, imperial: Fahrenheit.
const Protocol = "https:" // because in Webkit working geoAPI only with https
const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
let date = new Date();
let city = "";
let country = "";

// Array for months, number months, year
function getWeekDay(days) {
    let term = (date.getDay() + days) % 7;
    return weekdays[term];
}
// Array for months, number months, year
function getDate(timestamp) {
    let d = new Date(timestamp * 1000);
    let term = d.getDate();
    let monthName = months[d.getMonth()];
    let monthNumber = d.getMonth()+1;
    let year = d.getFullYear();
    let dateArray = [term, monthName, monthNumber, year];
    return dateArray;
}
// Centering the loader
function startLoading() {
  let cssLoader = $(".cssload-loader");
  cssLoader.show();
  let centerY = (($(window).height() - cssLoader.height())/2)+$(window).scrollTop();
  let centerX = (($(window).width() - cssLoader.width())/2)+$(window).scrollLeft();
   cssLoader.offset({top:centerY, left:centerX});
}

function stopLoading() {
  $(".cssload-loader").hide();
}

jQuery(function() {
    // Place identification using GoogleMaps
    function geoApi() {
        navigator.geolocation.getCurrentPosition(success, error);
        function success(position) {
            let urlGeocode = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + position.coords.latitude + "%2C" + position.coords.longitude + "&language=en";
            $.getJSON(urlGeocode, false).done(function(location) {
                let results = location.results[0];
                //console.log(results);
                city = results.address_components[4].long_name;
                country = results.address_components[6].long_name;
                document.querySelector(".information .method").innerHTML = "Geo(GPS)";
                document.querySelector("#country").innerHTML = country;
                document.querySelector("#region").innerHTML = results.address_components[5].long_name;
                document.querySelector("#city").innerHTML = city;
                document.querySelector("#latitude").innerHTML = position.coords.latitude;
                document.querySelector("#longitude").innerHTML = position.coords.longitude;
                document.querySelector("#district").innerHTML = "Your district: <span>"+results.address_components[2].long_name+"</span>";
                document.querySelector("#address").innerHTML = "Your address: <span>"+results.formatted_address+"</span>";
                sessionStorage.setItem('City', city);
                sessionStorage.setItem('Country', country);
            });
        }
        function error(err) {
            alert("Geolocation API doesn't work!");
            console.log(err);
        }
    }

    function geoIp() {
        // Place identification using the service - ipinfo.io
        $.get("http://ipinfo.io", onAjaxSuccess, "jsonp");
            startLoading();
        function onAjaxSuccess (response) {
            city = response.city;
            country = response.country;
            let loc = response.loc.split(',');
            document.querySelector(".information .method").innerHTML = "IP";
            document.querySelector("#country").innerHTML = country;
            document.querySelector("#region").innerHTML = response.region;
            document.querySelector("#city").innerHTML = city;
            document.querySelector("#latitude").innerHTML = loc[0];
            document.querySelector("#longitude").innerHTML = loc[1];
            sessionStorage.setItem('City', city);
            sessionStorage.setItem('Country', country);
            stopLoading();
        }
    }

    if (navigator.geolocation) {
        if (location.protocol == Protocol) {
            geoApi();
        } else {
            geoIp();
        }
    } else {
        geoIp();
    }
});

jQuery(window).load(function() {
    let place = sessionStorage.getItem('City');
    let code = sessionStorage.getItem('Country');
    if (location.protocol == Protocol) {
        // Replace of letters (for example Kraków)
        if( ~place.indexOf('ó') ){
            place = place.replace(/ó/gi, 'o');
        }
        //console.log(place);
    }

    let timearry = [];
    let temparry = [];
    let namearry = [];
    let array = [];
    let urlApi = "http://api.openweathermap.org/data/2.5/forecast?q="+ place +","+ code +"&units="+ Unit +"&appid="+ APIKey;
       // Using API openweathermap.org for forecast
       $.get(urlApi, function (data) {
           //$("#city2").html(data.city.name);
           array = data.list;
           for(let i = 0; i < 40; i+=8){
               let time = array[i].dt;
               let temp = array[i].main.temp;
               let weather = array[i].weather[0].description;
               // Array time
               timearry.push(time);
               // Rounding of temperature
               temp = temp.toFixed(1);
               temparry.push(temp);
               // Array weather
               weather = weather.replace(" ", '_');
               if(weather == "drizzle_rain"){
                   weather = "light_rain";
               }
               if( weather == "shower_rain" || weather == "heavy_intensity_rain" ||  weather == "very_heavy_rain"){
                   weather = "rain";
               }
               namearry.push(weather);
           }
           // Time for infoblock
           let nowTimeAll = array[0].dt_txt;
           let nowTime = nowTimeAll.split(' ');
           nowTime = nowTime[1].slice(0, -3);
           let nowDate = getDate(timearry[0]);
           document.querySelector("#actual").innerHTML = nowTime +" "+ nowDate[0] + "<sup>th</sup> of " + nowDate[1] + " " + nowDate[3];
           document.querySelector("#weather").innerHTML = namearry[0].replace("_", ' ');

           for(let i = 0; i < 5; i++){
               let stampArray = getDate(timearry[i]);

               $("#day-"+i).find(".slide__element--date").html(getWeekDay(i)+ ", " +stampArray[0]+ "<sup>th</sup> of " + stampArray[1]+ " " +stampArray[3]);
               $("#day-"+i).find(".slide__element--temp").html(temparry[i]+ "°<small>C</small>");

               let ico = $("a[href='#day-"+i+"']").find(".icon");
                   ico.next().html(stampArray[2] + "/" + stampArray[0]);
                   ico.addClass(namearry[i]);

               document.querySelector("#day-"+i).dataset.weather = namearry[i];
           }
       }, "jsonp");

    // Animation for button of smartphones
    $("#show").on("click", function() {
        $(this).find(".icon").removeClass("icon-menu");
            $(".information").fadeIn();
    });
    $("#hide").on("click", function() {
        $(".information").fadeOut();
            setTimeout(function() {
                $("#show").find(".icon").addClass("icon-menu");
                    }, 500);
    });
});
